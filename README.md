# Big Data in Finance 

## Part IV: CRSP and Compustat: an Application to Quant Finance

### Author: Lira Mota 

### Version: 0.3

This repository contains class material for Part IV of Big Data in Finance (Spring 2021).

# Topics
1. Compustat
2. CRSP
3. Factor Replication
4. Quant Investing 